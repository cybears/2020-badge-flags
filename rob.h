template<typename Tag>
struct robbed {
    typedef typename Tag::type type;
    static type ptr;
};

template<typename Tag>
typename robbed<Tag>::type robbed<Tag>::ptr;

template<typename Tag, typename Tag::type p>
struct rob : robbed<Tag> {
    struct filler {
        filler() { robbed<Tag>::ptr = p; }
    };
    static filler filler_obj;
};

template<typename Tag, typename Tag::type p>
typename rob<Tag, p>::filler rob<Tag, p>::filler_obj;
