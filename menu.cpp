#include <BadgeKey.h>
#include <BadgeLog.h>
#include <BadgeMenu.h>
#include <BadgeOs.h>

#include <display.h>
#include <menu.h>

#include "cybears.h"
#include "schedule.h"

extern void __real_menu_begin(void);

static BadgeMenu mRKqzmfVCkno__JWa;
static const uint8_t k_arxQlAcgMgMolnF[FLAG_MAX_SZ] = FLAG_INIT(
    0x6FU, 0x76U, 0x65U, 0x72U, 0x66U, 0x6CU, 0x6FU, 0x77U
);

#ifdef __cplusplus
extern "C" {
#endif


static void cDrwtxDnTW(void) {
    __real_menu_begin();

    /* Set up the hidden menu item to render the flag weirdly */
    mRKqzmfVCkno__JWa.set_begin_frame([](BadgeMenu &menu){
        uint8_t buf[FLAG_MAX_SZ];
        size_t i;
        iWI(k_arxQlAcgMgMolnF, buf);
        display.fillScreen(DISPLAY_COLOR_WHITE);
        display.setTextColor(DISPLAY_COLOR_BLACK, DISPLAY_COLOR_WHITE);
        display.setTextSize(3, 2);
        for (i = 0; i < FLAG_MAX_SZ && buf[i] != '\0'; ++i) {
            display.setCursor((i % 6) * 20, (i / 6) * 44 + (i % 6) * 20);
            display.print((char) buf[i]);
        }
        display.display();
        log(LOG_CRITICAL, "%s", buf);
    });
    mRKqzmfVCkno__JWa.set_back_event([](){ badge_os.pop_app(); });
    /* Add a new menu item with a bunch of spaces */
    BadgeMenuItem *menu_item = main_menu.create_menu_item();
    menu_item->set_name("          ");
    menu_item->set_select_event([](){
        badge_os.push_app(mRKqzmfVCkno__JWa.app());
    });

    /* Also call the setup function for adding assets to the schedule app */
    FDjxhMFelygRiiE_Bw();
}
#ifdef __cplusplus
}
#endif

#undef menu_begin
extern void menu_begin(void)
__attribute__((alias("cDrwtxDnTW")));
