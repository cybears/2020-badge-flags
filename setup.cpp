#include <BadgeLog.h>

#include "cybears.h"

extern "C" {

extern __attribute__((constructor(1337))) void owfg_Z(void) {
    /* We want this to be a stack built string so we can't really initialise it
     * but we can use the encoder to screw it up and then log it as a hint.
     *
     * Leading NUL to leave the leading char of the flag unchanged when we
     * "decode".
     */
    uint8_t bPp[FLAG_MAX_SZ] = {
        0x00U,
        0x63U, 0x79U, 0x62U, 0x65U, 0x61U, 0x72U, 0x73U, 0x7bU,
        0x26U, 0x2dU, 0x31U, 0x6dU, 0x2dU, 0x30U, 0x4eU, 0x5fU,
        0x55U, 0x72U, 0x2dU, 0x35U, 0x2bU, 0x34U, 0x28U, 0x6bU,
        0x7dU,
    };
    iWI(bPp, bPp);
    log(LOG_WARN, "CORRUPT MESSAGE");
    for (size_t i = 0; (i + 1) * 8U < sizeof(bPp); ++i) {
        log(
            LOG_INFO,
            "%02X %02X %02X %02X %02X %02X %02X %02X",
            bPp[i*8+0], bPp[i*8+1], bPp[i*8+2], bPp[i*8+3],
            bPp[i*8+4], bPp[i*8+5], bPp[i*8+6], bPp[i*8+7]
        );
    }
}

};
