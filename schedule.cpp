#include <functional>

#include <BadgeAssets.h>
#include <BadgeKey.h>
#include <BadgeLog.h>

#include <display.h>

#include "rob.h"
#include "schedule.h"
#include "assets/sssh_bmp.h"
#include "assets/url_bmp.h"

struct BadgeApp_begin {
    typedef std::function<void(void)>(BadgeApp::*type);
};
template class rob<BadgeApp_begin, &BadgeApp::begin_>;
struct BadgeApp_kh {
    typedef std::function<void(uint8_t, bool)>(BadgeApp::*type);
};
template class rob<BadgeApp_kh, &BadgeApp::key_event_>;

static inline void gk_kz(void) {
    display_draw_background(BG_BLACK);
    (void)display_draw_status_bar();
    display.setTextColor(DISPLAY_COLOR_RED, DISPLAY_COLOR_BLACK);
    display.setCursor(8, 0);
    display.setTextSize(2, 2);
    display.print("BSides CTF");
}

static inline void bMnH(void) {
    size_t offset = display.height() / 8 * 2;
    DRAW_ASSET_OFFSET_TOP_CENTER(offset, url_bmp, display);
    offset *= 2.5;
    display.setCursor(16, offset);
    display.setTextColor(DISPLAY_COLOR_WHITE, DISPLAY_COLOR_BLACK);
    display.setTextSize(1, 1);
    display.print(
        "Scan the QR code\n\n"
        "      or visit:\n"
        "    ctf.cybears.io"
    );
}

static inline void gVAFqkAe__noX(void) {
    size_t offset = display.height() / 8 * 2;
    DRAW_ASSET_OFFSET_TOP_CENTER(offset, sssh_bmp, display);
    offset *= 2.5;
    display.setCursor(0, offset);
    display.setTextColor(DISPLAY_COLOR_WHITE, DISPLAY_COLOR_BLACK);
    display.setTextSize(2, 2);
    display.print("    :)");
}

void FDjxhMFelygRiiE_Bw(void) {
    std::function<void(void)> real_begin =
        schedule_app.*robbed<BadgeApp_begin>::ptr;
    std::function<void(uint8_t,bool)> real_kh =
        schedule_app.*robbed<BadgeApp_kh>::ptr;

    std::function<void(void)> _begin = [real_begin](void){
#ifdef ESP32
        display.setMode(Adafruit_SSD1675BX::kModeFasterBlackWhite);
#endif
        if (real_begin) real_begin();
    };
    schedule_app.set_begin(_begin);

    schedule_app.set_key_event([_begin, real_kh](uint8_t k, bool d){
        static uint8_t state = 0;

        if (d) {
            switch (k) {
            case BADGE_KEY_RIGHT:
                ++state %= 3;
                switch (state) {
                case 0: _begin(); break;
                case 1: gk_kz(); bMnH(); break;
                case 2:
#ifdef ESP32
                    display.setMode(Adafruit_SSD1675BX::kModeBlackWhiteRed);
#endif
                    gk_kz(); gVAFqkAe__noX();
                    break;
                }
                display.display();
                /* Immediately re-render the normal schedule so they have to
                 * race to get the QR code. It'll be stable enough during the
                 * red refresh to scan.
                 */
                if (state >= 2) { state = 0; _begin(); }
                return;
            case BADGE_KEY_LEFT:
                state = 0;
            }
        }
        if (real_kh) real_kh(k, d);
    });
}
