import shutil
import sys

from flags import FLAGS

FILE_TO_APPEND = sys.argv[1]
BACKUP_NAME = FILE_TO_APPEND + ".unappended"

FLAG_NAME = b"its_dark"
FLAG_DATA = FLAGS[FLAG_NAME]

# We add padding to push the flag out into what looks like erased flash
SEGMENT_SIZE = 512  # IDK if this is actually right but it seems sensible
# Make a chunk of bytes which looks like an erased segment
ERASED_SEGMENT_DATA = b"\xff" * SEGMENT_SIZE
# This is the number of segments we'll push the flag out to
FLAG_SEGMENT_OFFSET = 128

# Make a backup copy
shutil.copyfile(FILE_TO_APPEND, FILE_TO_APPEND + ".unappended")

with open(FILE_TO_APPEND, "ab") as f:
    bin_sz = f.tell()
    # Pad out to the next segment boundary (assuming flash offset is aligned)
    f.write(ERASED_SEGMENT_DATA[bin_sz % SEGMENT_SIZE:])
    for _ in range(FLAG_SEGMENT_OFFSET):
        f.write(ERASED_SEGMENT_DATA)
    # Now write the actual flag
    f.write(FLAG_DATA)
    # Pad out the rest of the segment just in case
    f.write(ERASED_SEGMENT_DATA[len(FLAG_DATA):])
