#include <functional>

#include <BadgeKey.h>
#include <BadgeLog.h>
#include <BSidesMessaging.h>

#include <display.h>
#include <device_info.h>

#include "cybears.h"
#include "rob.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

extern void __real_device_info_begin(void);

struct BadgeApp_kh {
    typedef std::function<void(uint8_t, bool)>(BadgeApp::*type);
};
template class rob<BadgeApp_kh, &BadgeApp::key_event_>;

extern SerialMessageQueue serialBridgeQueue;

#ifdef __cplusplus
extern "C" {
#endif

static const uint8_t oVWtQZsIDd[FLAG_MAX_SZ] = FLAG_INIT(0x35, 0x37, 0x33);

static void Tr_eahGpxiHxAwMXigwEI(void) {
    __real_device_info_begin();

    std::function<void(uint8_t,bool)> real_kh =
        device_info.*robbed<BadgeApp_kh>::ptr;
    device_info.set_key_event([real_kh](uint8_t k, bool d){
        static uint8_t code[] = {
            BADGE_KEY_UP, BADGE_KEY_UP,
            BADGE_KEY_DOWN, BADGE_KEY_DOWN,
            BADGE_KEY_LEFT, BADGE_KEY_RIGHT,
            BADGE_KEY_LEFT, BADGE_KEY_RIGHT,
            BADGE_KEY_ENTER,
            /* TERMINATOR */ _BADGE_KEY_INVALID,
        };
        static size_t i = 0;
        static SerialMessage mtx;

        if (d && BADGE_KEY_IS_DPAD(k)) {
            /* Advance the cursor if we match, otherwise reset and if the
             * entered key is `LEFT` then go back as usual.
             */
            RGBLEDState s = { 0 };
            if (i < ARRAY_SIZE(code) && code[i] == k) {
                s.blue[i] = 0xFF; s.brightness[i] = 0x7F;
                i++;
            } else {
                i = 0;
                /* Only call the real handle for keys we care about if we get
                 * reset by an incorrect input.
                 */
                real_kh(k, d);
            }
            /* If we've walked to the tail item then they've won so we'll
             * decode, render and log the flag.
             */
            if (i >= ARRAY_SIZE(code) - 1) {
                uint8_t buf[FLAG_MAX_SZ];
                iWI(oVWtQZsIDd, buf);
                display.fillScreen(DISPLAY_COLOR_BLACK);
                display.setTextColor(DISPLAY_COLOR_WHITE, DISPLAY_COLOR_BLACK);
                display.setCursor(0, 0);
                display.setTextSize(2, 2);
                display.println((char *) buf);
                display.display();
                log(LOG_CRITICAL, "%s", buf);
                /* Reset the state machine for next time */
                i = 0;
            }
            /* Send the LEDSet message */
            if (prepareMessage(
                mtx, LEDSet, nextSequenceNumber(), &s, sizeof(s)
            )) {
                (void) enqueueMessage(mtx, serialBridgeQueue);
            }
        } else {
            /* If we don't care about this event just pass it on */
            if (real_kh) real_kh(k, d);
        }
    });
}

#ifdef __cplusplus
}
#endif

#undef device_info_begin
extern void device_info_begin(void)
__attribute__((alias("Tr_eahGpxiHxAwMXigwEI")));
