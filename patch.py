import shutil
import sys

from flags import FLAGS

FILE_TO_PATCH = sys.argv[1]

# These must stay in sync with `cybears.h`
FLAG_MAX_SZ = 60
FLAG_MAGIC = b"jrUHiHLNfrEtnbSv"
FLAG_KEY_TERM = b"\xff\xff"

# Make a backup copy
shutil.copyfile(FILE_TO_PATCH, FILE_TO_PATCH + ".unpatched")

data = bytearray(open(FILE_TO_PATCH, "rb").read())
offset = None
while offset != -1:
    offset = data.find(FLAG_MAGIC, offset)
    if offset >= 0:
        key_s = offset + len(FLAG_MAGIC)
        key_e = data.find(FLAG_KEY_TERM, key_s)
        flag_key = bytes(data[key_s:key_e])
        try:
            flag_data = FLAGS[flag_key]
        except KeyError as exc:
            print(f"CYBEARS: No flag matching {flag_key!r}")
        else:
            patch_data = bytearray(FLAG_MAX_SZ)
            patch_data[:len(flag_data)] = flag_data
            print(f"CYBEARS: Patched {flag_key!r}")
            data[offset:offset + FLAG_MAX_SZ] = patch_data
        offset += 1
open(FILE_TO_PATCH, "wb").write(data)
