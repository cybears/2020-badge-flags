import secrets

FLAGS = {
    b"strhard": b"cybears{1m-iN_Ur-RT()5}",
    # This one isn't actually patched in since we need the compiler to make a
    # stack built string - included here for the record and should be kept
    # consistent with the byte buffer in `setup.cpp`. It would be nice to have
    # a pre-build step which creates a header file defining the array init so
    # we don't duplicate this flag data...
    b"strharder": b"cybears{&-1m-0N_Ur-5+4(k}",
    b"its_dark": b"cybears{1-h4v3_n0-m3m0rY_0F-th15_PL4c3}",
}
FLAGS_TO_OBFUSCATE = {
    b"573": b"cybears{c4pC0m-a1n+-nU+hiN}",
    # This flag gets rendered in such a way that dashes and underscores are
    # difficult to differentiate so we pick other symbols for word separation
    b"overflow": b"cybears{0nly%90s*k1Dz$r3meMbR}",
}

def encode(flag):
    # Randomising this every time is fine as long as we never have to patch a
    # flag in more than once with the exact same "ciphertext" data
    encoded = bytearray(secrets.token_bytes(1))
    if flag[-1] != b'\x00':
        flag += b'\x00'
    for b in flag:
        encoded.append(b)
        encoded[-1] ^= encoded[-2]
    return encoded

for k, v in FLAGS_TO_OBFUSCATE.items():
    FLAGS[k] = encode(v)
