#ifndef _CYBEARS_H
#define _CYBEARS_H

#include <stddef.h>
#include <stdint.h>

/* Pick a small-ish size not 8 byte aligned to make our buffers less
 * recognisable.  Basically we don't want someone to trivially spot the flags
 * by hunting for a `FLAG_MAX_SZ` byte buffer with mostly trailing NULs.
 *
 * Lucky for us, FLOSS doesn't work on anything except PEs right now!
 */
#define FLAG_MAX_SZ  (60U)

/* Magic string used to find patch points - 'jrUHiHLNfrEtnbSv' */
#define FLAG_INIT(...) {                                    \
    0x6AU, 0x72U, 0x55U, 0x48U, 0x69U, 0x48U, 0x4CU, 0x4EU, \
    0x66U, 0x72U, 0x45U, 0x74U, 0x6EU, 0x62U, 0x53U, 0x76U, \
    __VA_ARGS__, 0xFFU, 0xFFU,                              \
}

/* Rolling XOR decoder - uses the first byte as an "IV" */
static inline __attribute__((always_inline))
void iWI(const volatile uint8_t* i, uint8_t* o) {
    size_t j;
    for (j = 0; j < FLAG_MAX_SZ - 2; ++j) {
        o[j] = i[j] ^ i[j + 1];
    }
    o[j] = '\0';
}

#endif//_CYBEARS_H
